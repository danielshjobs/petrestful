package com.qualityscore.pet.mock;


import com.qualityscore.pet.dto.AnimalDTO;
import com.qualityscore.pet.dto.PetDTO;
import com.qualityscore.pet.service.PetService;
import com.qualityscore.pet.web.PetController;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = PetController.class)
@AutoConfigureMockMvc
public class PetControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private PetService petService;
    @Test
    public void testfindPetByType() throws Exception {

        PetDTO petDTO = new PetDTO("https://dl5zpyw5k3jeb.cloudfront.net/photos/pets/59362267/1/?bust=1673002418&width=300",
                "Honey Pie","Chihuahua", "female","young");
        AnimalDTO animalDTO = new AnimalDTO("dog5", Arrays.asList(petDTO));
        Mockito.when(petService.findCatOrDog("dog", 5,"dog5")).thenReturn(Optional.of(animalDTO).get());
        mockMvc.perform(get("/api/v1/pet/").param("type", "dog").param("limit", String.valueOf(5)))
                .andExpect(status().isOk()).andExpect(jsonPath("$.pet", Matchers.hasSize(1)))
                .andExpect(jsonPath("$.id", Matchers.equalTo("dog5")))
                .andExpect(jsonPath("$.pet[0].name", Matchers.equalTo("Honey Pie")));
        Mockito.verify(petService, times(1)).findCatOrDog("dog", 5,"dog5");

    }

    @Test
    public void testfindAll() throws Exception {
        PetDTO petDTO = new PetDTO("https://dl5zpyw5k3jeb.cloudfront.net/photos/pets/59362267/1/?bust=1673002418&width=300",
                "Honey Pie","Chihuahua", "female","young");
        AnimalDTO animalDTO = new AnimalDTO("all", Arrays.asList(petDTO));
        Mockito.when(petService.findAll(false, 5)).thenReturn(Optional.of(animalDTO).get());
        mockMvc.perform(get("/api/v1/pet/").param("limit", String.valueOf(5)))
                .andExpect(status().isOk()).andExpect(jsonPath("$.pet", Matchers.hasSize(1)))
                .andExpect(jsonPath("$.id", Matchers.equalTo("all")))
                .andExpect(jsonPath("$.pet[0].name", Matchers.equalTo("Honey Pie")));
        Mockito.verify(petService, times(1)).findAll(false,5);

    }


}
