package com.qualityscore.pet.config;

import com.qualityscore.pet.dto.Jwt;
import com.qualityscore.pet.dto.Oauth2;
import com.qualityscore.pet.entity.Animal;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
@Data
public class InitPetApi {
    private final RestTemplate restTemplate;
    @Value("${api.host.client}")
    private String client;
    @Value("${api.host.secret}")
    private String secret;
    @Value("${api.host.type}")
    private String type;
    private Jwt jwt;

    @PostConstruct
    public void initToken(){
        Oauth2 oauth2 = new Oauth2(type, client, secret);
        ResponseEntity<Jwt> result = restTemplate.postForEntity("oauth2/token", oauth2, Jwt.class);
        System.out.println(result.getBody());
        this.jwt = new Jwt(result.getBody().getTokenType(), result.getBody().getAccessToken());

    }

}
