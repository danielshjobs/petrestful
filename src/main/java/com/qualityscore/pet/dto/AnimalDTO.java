package com.qualityscore.pet.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AnimalDTO implements Serializable {
    private String id;
    private List<PetDTO> pet;
}
