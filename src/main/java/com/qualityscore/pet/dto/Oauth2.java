package com.qualityscore.pet.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Oauth2 {

    @JsonProperty("grant_type")
    private String granType;
    @JsonProperty("client_id")
    private String clientId;
    @JsonProperty("client_secret")
    private String client_secret;
}

