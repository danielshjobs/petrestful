package com.qualityscore.pet.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@RequiredArgsConstructor
@Data
public class PetDTO implements Serializable {

    private String image;
    private String name;
    private String primaryBreed;
    private String gender;
    private String age;

}


