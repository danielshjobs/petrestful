package com.qualityscore.pet.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Pet {

    @JsonProperty("primary_photo_cropped")
    private Photo primary;
    private String name;
    @JsonProperty("breeds")
    private Breed breed;
    private String gender;
    private String age;
}
