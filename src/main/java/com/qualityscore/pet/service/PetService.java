package com.qualityscore.pet.service;

import com.qualityscore.pet.config.InitPetApi;
import com.qualityscore.pet.dao.PetDAO;
import com.qualityscore.pet.dto.AnimalDTO;
import com.qualityscore.pet.dto.PetDTO;
import com.qualityscore.pet.entity.Animal;
import com.qualityscore.pet.entity.Pet;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PetService {

    private final RestTemplate restTemplate;
    private final PetDAO petDAO;
    private final InitPetApi initPetApi;


    @Cacheable(cacheNames = "pets",  condition = "!#nocache")
    public AnimalDTO findAll(boolean nocache, Integer limit){List<List<Pet>> allPets = new ArrayList<>();
       List<Pet> dogList = findByType("dog", limit).getAnimals();
       List<Pet> catList = findByType("cat", limit).getAnimals();
       if(limit < 5) {
           dogList = dogList.stream().skip(0).limit(4).collect(Collectors.toList());
           catList = catList.stream().skip(0).limit(4).collect(Collectors.toList());
           allPets = Arrays.asList(dogList,catList);
       }else {
           allPets = Arrays.asList(dogList,catList);
       }
       List<PetDTO> petDTO = petDAO.mapPetListToListPetDTO(allPets);
       return new AnimalDTO("all", petDTO);
    }

    @Cacheable(cacheNames = "pets", key = "#id")
    public AnimalDTO findCatOrDog(String type, Integer limit, String id){
        List<Pet> pets = findByType(type, limit).getAnimals();
        return new AnimalDTO(id, petDAO.mapPetToPetDTO(pets, limit));
    }

    public Animal findByType(String type, Integer limit){
        if(limit < 20)
            limit=20;
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization",  initPetApi.getJwt().getTokenType()+" " + initPetApi.getJwt().getAccessToken());
        HttpEntity<String> request = new HttpEntity<String>(headers);
        ResponseEntity<Animal> animals = restTemplate.exchange("animals?type="+type+"&country=us&limit="+limit+"", HttpMethod.GET,request, Animal.class);
        return animals.getBody();
    }

    //Clear of all cache entries
    @CacheEvict(cacheNames = "pets", allEntries = true)
    public void clearCache() {
    }
}
