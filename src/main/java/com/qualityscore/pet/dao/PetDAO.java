package com.qualityscore.pet.dao;

import com.qualityscore.pet.dto.PetDTO;
import com.qualityscore.pet.entity.Pet;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.stream.Collectors;
@Repository
public class PetDAO {
    public List<PetDTO> mapPetListToListPetDTO(List<List<Pet>> pet){
        return pet.stream().flatMap(pets-> pets.stream())
                .map(x->{
                    if(x.getPrimary() != null )
                        return new PetDTO(x.getPrimary().getImage(),x.getName(),x.getBreed().getPrimaryBreed(),x.getGender(), x.getAge());
                    else
                        return new PetDTO("", x.getName(), x.getBreed().getPrimaryBreed(),x.getGender(), x.getAge());
                }).collect(Collectors.toList());
    }

    public List<PetDTO> mapPetToPetDTO(List<Pet> pet, Integer limit){
        return pet.stream().map(x->{
            if(x.getPrimary() != null )
                return new PetDTO(x.getPrimary().getImage(),x.getName(),x.getBreed().getPrimaryBreed(),x.getGender(), x.getAge());
            else
                return new PetDTO("", x.getName(), x.getBreed().getPrimaryBreed(),x.getGender(), x.getAge());
        }).limit(limit).collect(Collectors.toList());
    }



}
