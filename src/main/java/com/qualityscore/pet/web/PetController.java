package com.qualityscore.pet.web;

import com.qualityscore.pet.dto.AnimalDTO;
import com.qualityscore.pet.dto.PetDTO;
import com.qualityscore.pet.service.PetService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.Max;


@RestController
@CrossOrigin
@RequestMapping("api/v1/pet")
@RequiredArgsConstructor
@Validated
public class PetController {
    private final PetService petService;

    @ApiOperation(value = "View of all the available animals" ,response = PetDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @GetMapping("/")
    public ResponseEntity<AnimalDTO> findAll(@RequestParam(required = false) String type,
                                             @RequestParam(required = false) boolean nocache,
                                             @RequestParam(required = false) @Max(100) Integer limit){
        if(limit == null )
            limit=20;
        if(type != null && !type.isEmpty()) {
            return ResponseEntity.ok(petService.findCatOrDog(type, limit, type+limit));
        }
        return ResponseEntity.ok(petService.findAll(nocache, limit));
    }

    @DeleteMapping("/cache")
    public void clearCache() {
        petService.clearCache();
    }

}
