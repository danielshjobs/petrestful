# Pet micro service - Provided A pet REST API application:


## Getting started

At a high level, pet micro service goal is to reduce identical requests from PetFinder API and provide a full customer support with all pet data. The pet microservice takes responsibility for the integration with petFinder Api and relies on caching (EHCACHE 3), to avoid being hit frequently by identical requests.

Pet microservice provides the relevant DTOs as needed, for a smooth and convenient experience.


## What Pet micro service includes
- Pet API includes a Rest API application and exposing an endpoint to the client with all our pet's data.
- Pet API is based on popular layers by the most popular patterns such as :(entity, DAO, DTO, unit test, Service, error handler...)
- Pet API includes variety of artifacts such as: (Spring web, Acutator, Swagger interface..)
   

## To Install:

 - Java 17
 - Maven 3.1+   
 - IDE recommended intellij comunity (the JDK & Maven should be build-in) 

## Run the app:

- Click Run in the IDE.   

## Run test:

- mvn test or /.mvnw test
 
## Run clean:      

- mvn clean

## Run Package:
   
- mvn Package   

## To Deploy & build a docker image

- To build docker image please run the command below -
     mvn jib:build -Djib.to.auth.username=$DOCKER_USER -Djib.to.auth.password=$DOCKER_PASSWORD -Djib.to.image.=$DOCKER_HOST 

- To deploy please assist by Helm 3 there is ready pet chart for your convenience, the command is: helm upgrade -i "Release" helm/Pet 



## REST API - endpoints details:

-  Get   /api/v1/pet/ - To retrieve all pets.
-  Get   /api/v1/pet/?type=cat - To retrieve all cats.
-  Get   /api/v1/pet/?type=dog - To retrieve all dogs.
-  Get   /api/v1/pet/?limit=20 - To retrieve all pets with 20 records as a limitation.
-  Get   /api/v1/pet/?limit=20&type=cat - To retrieve cats / dogs with 20 records as a limitation.


for your convinience, please assist by swagger-ui to invoke the api, below the pet swagger path:
 -  http://localhost:8080/swagger-ui/index.html



 




